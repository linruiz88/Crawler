﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PttCrawler.Model
{
    class ModelComment
    {
        public string link { get; set; }
        public string maker { get; set; }
        public string title { get; set; }
        public string date { get; set; }
        public string content { get; set; }
        public List<ModelRecall> recall { get; set; }
    }
}
