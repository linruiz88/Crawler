﻿using PttCrawler.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PttCrawler
{
    public partial class Launch : ServiceBase
    {
        public Launch()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Crawler c = new Crawler();
            c.Run();
        }

        protected override void OnStop()
        {

        }

        public void Start(string[] args)
        {
            this.OnStart(args);
        }

        public void Stop()
        {
            this.OnStop();
        }


    }
}
