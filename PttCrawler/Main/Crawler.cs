﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZapLib;
using HtmlAgilityPack;
using PttCrawler.Model;


namespace PttCrawler.Main
{
    class Crawler
    {
        public void Run()
        {
            /* Set Target Website URL */
            string uri = Config.get("Uri");
            int page = Int32.Parse(Config.get("page")); // 取得參數
            var baseUrl = Config.get("pttUri");/* Send HTTP Request and Get Result */
            HtmlWeb web = new HtmlWeb();

            string next = "https://www.ptt.cc/bbs/C_Chat/index.html";

            for (int a = 0; a < page; a++)  //爬 ? 頁
            {
                int s = 1; //第一頁 預設

                var htmlDoc = web.Load(next);
                if (htmlDoc == null)
                    Console.WriteLine("fail");
                else
                    Console.WriteLine("成功");


                SQL db = new SQL(); //連線資料庫


                var content = htmlDoc.DocumentNode.SelectSingleNode("//div[@class=\"r-ent\"]"); //文章內容
                var link = content.SelectNodes("//div[@class=\"title\"]//a");
                var btn = htmlDoc.DocumentNode.SelectSingleNode("//div[@class=\"action-bar\"]"); //標題內容
                var nex = btn.SelectNodes("//div[@class=\"btn-group btn-group-paging\"]//a[2]");


                ModelPost m_post = new ModelPost();// 建構資料型態Model




                foreach (var n in link) //取得文章與連結
                {
                    m_post.name = n.InnerText;
                    m_post.link = uri + n.Attributes["href"].Value;

                    Console.WriteLine("\n -----第 " + s + " 篇開始-----");
                    s++; 
                   // Console.WriteLine(m_post.name + "\n" + m_post.link);


                    enter(m_post.link); // 副程式進入點 

                    //寫入資料庫
                    string sql = (m_post.name != null && m_post.link != null ) ?
                    @"insert into ptt(name,link,next)values(@name,@link,@next)" : "select * from ptt";

                    var para = new
                    {
                        name = m_post.name,
                        link = m_post.link,
                        next = baseUrl.ToString()
                    };
                    ModelPost[] data = db.quickQuery<ModelPost>(sql, para);

                    if (data == null)
                        Console.WriteLine("SQL is fail");
                    //Console.Write(" ----  Access  ---- ");

                }

                foreach (var n in nex) //取得下一頁PTT列表
                {
                    if (n.NodeType == HtmlNodeType.Element)
                    {
                        m_post.next = uri + n.Attributes["href"].Value;
                        Console.WriteLine("Next: " + uri + n.Attributes["href"].Value);
                    }
                }
                next = m_post.next; //取得下一頁

            }

        }

        public static string enter(string link)
        {
            string a, b, c, d, e, f = null;

            HtmlWeb web = new HtmlWeb();
            var htmlDoc = web.Load(link);

                if (htmlDoc == null)
                    Console.WriteLine("fail");
                else
                    Console.WriteLine(" ");

            //Console.WriteLine(link);
            var content = htmlDoc.DocumentNode.SelectSingleNode("//div[@id=\"main-container\"]"); //取得文章\
            var top = htmlDoc.DocumentNode.SelectSingleNode("//div[@id=\"main-content\"]"); //文章ALL
            var maker = top.SelectNodes("//div[@class=\"article-metaline\"]"); //取得標題列表
            var titler = top.SelectNodes("//div[@class=\"article-metaline-right\"]"); //標題開頭
            var recall = top.SelectNodes("//div[@class=\"push\"]"); //取得回覆留言

    //連線資料庫
            ModelComment m_comment = new ModelComment();
            ModelRecall m_recall = new ModelRecall();
            SQL db = new SQL();

            if (recall != null)
            {
                foreach (var n in recall)
                {
                    if (n.NodeType == HtmlNodeType.Element)
                    {
                        top.RemoveChild(n); //移除留言者 內容 時間

                        m_recall.re_caller = n.SelectSingleNode("span[2]").InnerText; //留言者
                        m_recall.re_text = n.SelectSingleNode("span[3]").InnerText;  //內容
                        m_recall.re_date = n.SelectSingleNode("span[4]").InnerText; //時間


                        //寫進資料庫 
                        string sql_recall = (m_recall.re_caller != null) ?
                         @"insert into recall(re_link,re_caller,re_text,re_date)values(@re_link,@re_caller,@re_text,@re_date)" :
                          "select * from ptt";

                        var par_recall = new
                        {
                            re_link = link,
                            re_caller = m_recall.re_caller,
                            re_text = m_recall.re_text,
                            re_date = m_recall.re_date

                        };

                        ModelRecall[] data = db.quickQuery<ModelRecall>(sql_recall, par_recall);
                        if (data == null)
                            Console.WriteLine(" SQL is Fail ");
                    }
                }
            }
            if (titler != null)
            {
                foreach (var n in titler)
                {
                    if (n.NodeType == HtmlNodeType.Element)
                    {
                        top.RemoveChild(n); //移除 看板 + C_CHAT

                        a = n.SelectSingleNode("span[1]").InnerText;
                        b = n.SelectSingleNode("span[2]").InnerText;
                        //Console.WriteLine(a+" "+b);
                    }
                }
            }

            if (maker != null)
            {
                foreach (var n in maker)
                {
                    if (n.NodeType == HtmlNodeType.Element)
                    {
                        top.RemoveChild(n);

                        switch (n.SelectSingleNode("span[1]").InnerText)
                        {
                            case "作者":
                                //Console.WriteLine("作者: " +n.SelectSingleNode("span[2]").InnerText); 
                                m_comment.maker = n.SelectSingleNode("span[2]").InnerText;

                                break;
                            case "標題":
                                //Console.WriteLine("標題: " +n.SelectSingleNode("span[2]").InnerText);
                                m_comment.title = n.SelectSingleNode("span[2]").InnerText;

                                break;
                            case "時間":
                                //Console.WriteLine("時間: " +n.SelectSingleNode("span[2]").InnerText);
                                m_comment.date = n.SelectSingleNode("span[2]").InnerText;
                                break;

                        }
                    }
                    m_comment.content = top.InnerText;
                };

                string sql_comment = (m_comment.maker != null) ?
                         @"insert into comment(link,maker,title,date,content)values(@link,@maker,@title,@date,@content)" :
                          "select * from ptt";
                var par_comment = new
                {
                    link = link,
                    maker = m_comment.maker,
                    title = m_comment.title,
                    date = m_comment.date,
                    content = m_comment.content
                };

                ModelComment[] data = db.quickQuery<ModelComment>(sql_comment, par_comment);
                if (data == null)
                    Console.WriteLine(" SQL is Fail ");
            
            }
            else
            {
                Console.WriteLine("    此頁為公告 因此為空值    ");
            }

            //Console.WriteLine(" 作者:" + m_comment.maker + "\n 標題:" + m_comment.title + "\n 日期:" + m_comment.date);
            //Console.WriteLine(top.InnerText);
            //Console.WriteLine(m_comment.recall);
            Console.WriteLine("-----此篇已結束-----");


            return f;
        }


    }
}
