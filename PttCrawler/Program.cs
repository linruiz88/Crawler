﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PttCrawler
{
    static class Program
    {
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                Launch s = new Launch();

                s.Start(null);

                Console.WriteLine("服務已啟動，請按下 Enter 鍵關閉服務...");
                // 必須要透過 Console.ReadLine(); 先停止程式執行
                // 因為 Windows Service 大多是利用多 Thread 或 Timer 執行長時間的工作
                // 所以雖然主執行緒停止執行了，但服務中的執行緒已經在運行了!
                Console.ReadLine();

                s.Stop();

                Console.WriteLine("服務已關閉");
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new Launch()
                };
                ServiceBase.Run(ServicesToRun);
            }

        }
    }
}
