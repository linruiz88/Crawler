# Ptt Crawler

    
    使用 $ git clone https://gitlab.com/linruiz88/Crawler.git 
    
    或
    
    fork 下載本專案
    

前置步驟

在 Visual Studio 2017 確定有以下工具
若沒有 , 請安裝 package 工具

 1. 使用nuget或下載安裝 [ZapLib](https://www.nuget.org/packages?q=zaplib)
 
 2. 使用nuget或下載安裝 [HtmlAgilityPack](https://www.nuget.org/packages/HtmlAgilityPack/)
 

 將 Visual Studio Community 
 執行介面由將原本的Windows Application 切換到 Console Application。
 
 P.S. 相關設定可以參考保哥這篇教學設定 [Console 小黑窗](https://blog.miniasp.com/post/2009/04/05/How-to-debugging-Windows-Service-and-Setup-Project-Custom-Action.aspx)
 
 
 
 可以讓同一個組件同時可以用來當 Windows Service 的主程式
 以及可以直接當成 Console 程式來執行，非常的方便。
 

在App.config 修改以下參數:


		 <add key "Ptturi" value="https://www.ptt.cc/bbs/C_Chat/index1.html" /> //PPT文章網址
		
		        // 限制: 本設計為任何一個PTT"最新"列表 如:C_Chat/index1 最新的第一頁，必須要能按上一頁
		
         < add key "page" value="1" />   // 要蒐集的頁數
         
                // 如: 要爬2 頁 , value 改成"2"
        

輸入完成後

即可以開執行爬搜



